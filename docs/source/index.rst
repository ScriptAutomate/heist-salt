.. heist-salt documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to heist-salt's Documentation!
======================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/heist-salt
   tutorial/index
   releases/index

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   GitLab Repository <https://gitlab.com/saltstack/pop/heist-salt>

Indices and tables
==================

* :ref:`modindex`
