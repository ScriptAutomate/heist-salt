import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="config")
    hub.pop.sub.add(dyne_name="heist")
    hub.pop.sub.add(dyne_name="salt")
    hub.pop.sub.add(dyne_name="artifact")
    yield hub
