import tempfile

import yaml


def mk_config(hub, root_dir: str, t_name):
    """
    Create a config to use with this execution and return the file path
    for said config
    """
    _, path = tempfile.mkstemp()
    roster = hub.heist.ROSTERS[t_name]

    config = {
        "master": roster.get("master", "127.0.0.1"),
        "master_port": roster.get("master_port", 44506),
        "publish_port": roster.get("publish_port", 44505),
        "root_dir": root_dir,
    }

    minion_opts = roster.get("minion_opts")
    if minion_opts:
        for key, value in minion_opts.items():
            # Use configurations set by user
            config[key] = value

    with open(path, "w+") as wfp:
        yaml.safe_dump(config, wfp)

    return path
