import asyncio
import copy
import os
import pathlib
import secrets
import sys
from typing import Any
from typing import Dict


async def run(
    hub,
    remotes: Dict[str, Dict[str, str]],
    artifacts_dir: str,
    artifact_version=None,
    dynamic_upgrade: bool = False,
    checkin_time: int = 60,
    **kwargs,
):
    coros = []

    if "accept_keys" not in kwargs:
        kwargs["accept_keys"] = hub.OPT.heist.get("accept_keys", False)

    if "key_plugin" not in kwargs:
        kwargs["key_plugin"] = hub.OPT.heist.get("key_plugin", "local_master")

    for id_, remote in remotes.items():
        coro = hub.heist.salt.minion.single(
            remote,
            artifacts_dir,
            artifact_version=artifact_version,
            dynamic_upgrade=dynamic_upgrade,
            checkin_time=checkin_time,
            accept_keys=kwargs["accept_keys"],
            key_plugin=kwargs["key_plugin"],
        )
        coros.append(coro)
    await asyncio.gather(*coros, loop=hub.pop.Loop, return_exceptions=False)


async def single(
    hub,
    remote: Dict[str, Any],
    artifacts_dir,
    artifact_version=None,
    dynamic_upgrade: bool = False,
    checkin_time: int = 60,
    accept_keys: bool = False,
    key_plugin: str = "local_master",
):
    """
    Execute a single async connection
    """
    # create tunnel
    target_name = secrets.token_hex()
    hub.heist.ROSTERS[target_name] = copy.copy(remote)
    t_type = remote.get("tunnel", "asyncssh")
    created = await hub.tunnel[t_type].create(target_name, remote)
    if not created:
        hub.log.error(f'Connection to host {remote["host"]} failed')
        return

    t_grains = await hub.heist.grains.get(remote, target_name=target_name)
    t_os = t_grains.kernel.lower()

    # create artifacts directory
    pathlib.Path(artifacts_dir).mkdir(parents=True, exist_ok=True)

    ver = await hub.artifact.salt.version(t_os, artifact_version)
    if ver:
        await hub.artifact.salt.get(target_name, t_type, artifacts_dir, t_os, ver=ver)

    # Get salt minion user
    user = hub.heist.ROSTERS[target_name].get("username")
    if not user:
        user = hub.heist.init.default(t_os, "user")

    run_dir = hub.heist.CONS[target_name].get("run_dir")
    if not run_dir:
        run_dir = (
            pathlib.Path(os.sep, "var")
            / "tmp"
            / f"heist_{user}"
            / f"{secrets.token_hex()[:4]}"
        )
        hub.heist.CONS[target_name]["run_dir"] = run_dir

    # Deploy
    bin_ = hub.artifact.salt.latest("salt", artifacts_dir, version=ver)
    tgt = await hub.artifact.salt.deploy(target_name, t_type, run_dir, bin_)
    if not tgt:
        hub.log.error(f"Could not deploy the artifact to the target {remote['id']}")
        return False
    service_plugin = hub.service.init.get_service_plugin(remote, t_grains)

    hub.heist.CONS[target_name].update(
        {
            "t_type": t_type,
            "manager": "salt.minion",
            "bin": bin_,
            "tgt": tgt,
            "service_plugin": service_plugin,
            "key_plugin": key_plugin,
        }
    )

    # Create tunnel back to master
    if not hub.heist.ROSTERS[target_name].get("bootstrap"):
        await hub.tunnel[t_type].tunnel(target_name, 44505, 4505)
        await hub.tunnel[t_type].tunnel(target_name, 44506, 4506)

    # Start minion
    hub.log.debug(f"Target '{remote.id}' is using service plugin: {service_plugin}")
    await hub.service.salt.minion.apply_service_config(
        t_type, target_name, run_dir, tgt, service_plugin
    )
    await hub.service[service_plugin].start(
        t_type, target_name, "salt-minion", block=False
    )

    if accept_keys:
        await hub.salt.key.init.accept_key_master(target_name, t_type, tgt, run_dir)

    hub.log.debug(
        f"Starting infinite loop on {remote.id}.  Checkin time: {checkin_time}"
    )

    while True:
        await asyncio.sleep(checkin_time)
        if dynamic_upgrade:
            latest = hub.artifact.salt.latest("salt", artifacts_dir)
            if latest != bin_:
                bin_ = latest
                await hub.artifact.salt.update(
                    target_name, t_type, latest, tgt, run_dir
                )


async def clean(hub, target_name, tunnel_plugin, service_plugin, vals):
    """
    Clean up the connections
    """
    # clean up service files
    await hub.service.init.clean(
        target_name, tunnel_plugin, "salt-minion", service_plugin
    )
    # clean up run directory and artifact
    await hub.artifact.init.clean(target_name, tunnel_plugin)

    minion_id = hub.heist.CONS[target_name].get("minion_id")
    if minion_id:
        if not hub.salt.key.init.delete_minion(minion_id, vals["key_plugin"]):
            hub.log.error(f"Could not delete the key for minion: {minion_id}")
