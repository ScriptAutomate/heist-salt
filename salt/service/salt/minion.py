import os
import tempfile
import textwrap


async def apply_service_config(
    hub, tunnel_plugin, target_name, run_dir, tgt, service_plugin=None
):
    if not service_plugin:
        service_plugin = hub.service.init.get_service_plugin()

    await getattr(hub, f"service.salt.minion.{service_plugin}_conf")(
        tunnel_plugin, target_name, run_dir, tgt
    )


async def systemd_conf(hub, tunnel_plugin, target_name, run_dir, tgt):
    contents = textwrap.dedent(
        """\
                [Unit]
                Description=The Salt Minion
                Documentation=man:salt-minion(1) file:///usr/share/doc/salt/html/contents.html https://docs.saltproject.io/en/latest/contents.html
                After=network.target salt-master.service

                [Service]
                KillMode=process
                Type=notify
                NotifyAccess=all
                LimitNOFILE=8192
                ExecStart={tgt} minion --config-dir {conf} --pid-file={pfile}

                [Install]
                WantedBy=multi-user.target
                """
    )

    _, path = tempfile.mkstemp()
    with open(path, "w+") as wfp:
        wfp.write(
            contents.format(
                tgt=tgt,
                conf=os.path.join(run_dir, "conf"),
                pfile=os.path.join(run_dir, "pfile"),
            )
        )
    await hub.tunnel[tunnel_plugin].send(
        target_name,
        path,
        hub.service.init.service_conf_path("salt-minion", "systemd"),
    )

    await hub.tunnel[tunnel_plugin].cmd(target_name, f"systemctl daemon-reload")
