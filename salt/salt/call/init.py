import pathlib


async def get_id(hub, target_name, tunnel_plugin, tgt, run_dir):
    run_dir = pathlib.Path(run_dir) / run_dir
    return await hub.tunnel[tunnel_plugin].cmd(
        target_name, f"{tgt} call --config-dir {run_dir} --local grains.get id"
    )
