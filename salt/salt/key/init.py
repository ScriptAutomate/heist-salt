import asyncio


def accept_minion(hub, minion: str, key_plugin="local_master") -> bool:
    return hub.salt.key[key_plugin].accept_minion(minion)


def delete_minion(hub, minion: str, key_plugin) -> bool:
    return hub.salt.key[key_plugin].delete_minion(minion)


async def accept_key_master(
    hub, target_name, tunnel_plugin, tgt, run_dir, key_plugin="local_master"
):
    """
    Accept the minions key on the salt-master
    """
    hub.log.info("Querying minion id and attempting to accept the minion's key")
    ret = await hub.salt.call.init.get_id(target_name, tunnel_plugin, tgt, run_dir)
    if ret.returncode == 0:
        minion_id = ret.stdout.split()[1]
        retry_key_count = hub.OPT.heist.get("retry_key_count", 5)
        while retry_key_count > 0:
            if hub.salt.key.init.accept_minion(minion_id, key_plugin):
                break
            await asyncio.sleep(5)
            retry_key_count = retry_key_count - 1
        else:
            hub.log.error(f"Could not accept the key for the minion: {minion_id}")
            return False
        hub.heist.CONS[target_name].update(
            {
                "minion_id": minion_id,
            }
        )
        hub.log.info(f"Accepted the key for minion: {minion_id}")
        return True
    else:
        hub.log.error(
            "Could not query the minion id from grains. Not accepting the key"
        )
    return False
